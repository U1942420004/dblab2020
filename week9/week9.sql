create or replace view usa_costumers as
select customerID, CustomerName, ContactName
from customers
where Country = "USA";

select *
from usa_customers join orders on usa_customers.CustomerID = orders.CustomerID;

create view products_below_avg_price as
select ProductID, ProductName, Price
from Products;
where Price < (select avg(Price) from Product);

select * from products_below_avg_price;