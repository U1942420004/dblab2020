load data
infile "C:\\Users\\Alpaslan\\Downloads\\denormalized_movie_db.csv"
into table denormalized
columns terminated by';';

show variables like "secure_file_priv";

load data
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\denormalized_movie_db.csv"
into table denormalized
columns terminated by';';

select * from movie_db.denormalized;

insert into movies (movie_id,title,ranking,rating,year,votes,duration,oscars,budget)
select distinct movie_id,title,ranking,rating,year,votes,duration,oscars,budget
from denormalized;

insert into genres (movie_id,genre_name)
select distinct movie_id,genre_name
from denormalized;

insert into languages (movie_id,language_name)
select distinct movie_id, language_name
from denormalized;

insert into countries(country_id,country_name)
select distinct country_id, country_name
from denormalized;

insert into directors(director_id, country_id, director_name)
select distinct director_id, country_id, director_name
from denormalized;

insert into stars(star_id, country_id, star_name)
select distinct star_id, country_id, star_name
from denormalized;

insert into producer_countries(movie_id,country_id)
select distinct movie_id,country_id
from denormalized;

insert into movie_stars(movie_id,star_id)
select distinct movie_id,star_id
from denormalized;

insert into movie_directors(movie_id,director_id)
select distinct movie_id,director_id
from denormalized;